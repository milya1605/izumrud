<?php
$page = "news";
include "/config.php";
?>
<!doctype html>
<html>
<head>
	<title><?=$pages['title'];?></title>
	<link href="css/style.css" rel="stylesheet">
</head>

<body>
	<table id="header">
		<tr>
			<td>
				<a href="/"><img src="images/logo.png" alt="" class="logo"></a>
				<div class="logo-text">Кондитерский концерн <br> БАБАЕВСКИЙ</div>				
			</td>
		</tr>
	</table>
	<table id="container">
		<tr>
			<td id="menu" valign="top">
				<ul>
					<li><a href="/">Главная</a></li>
					<li><a href="/catalog.php">Каталог</a></li>
					<li><a href="/news.php">Новости</a></li>
					<li><a href="/gb.php">Гостевая книга</a></li>
					<li><a href="/kontakty.php">Контакты</a></li>
				</ul>

			</td>
			<td id="content" valign="top">
				<h1><?=$pages['h1'];?></h1>
				<? foreach($news as $new):?>
				<div class="news">
					<div class="header-new"><?=$new['header'];?></div>
					<div class="date-new"><?=$new['date'];?></div>
					<div class="description-new">
						<?=$new['description'];?>
					</div>
					
				</div>
				<? endforeach;?>
			</td>
		</tr>
		<tr>
			<td colspan="2"><hr></td>
		</tr>
		<tr id="footer">
			<td class="copy">2016 &copy; Кондитерский концерн "Бабаевский"</td>
		</tr>
	</table>
</body>
</html>