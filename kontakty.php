<?php
$page = "kontakty";
include "/config.php";
?>
<!doctype html>
<html>
<head>
	<title><?=$pages['title'];?></title>
	<link href="css/style.css" rel="stylesheet">
</head>

<body>
	<table id="header">
		<tr>
			<td>
				<a href="/"><img src="images/logo.png" alt="" class="logo"></a>
				<div class="logo-text">Кондитерский концерн <br> БАБАЕВСКИЙ</div>				
			</td>
		</tr>
	</table>
	<table id="container">
		<tr>
			<td id="menu" valign="top">
				<ul>
					<li><a href="/">Главная</a></li>
					<li><a href="/catalog.php">Каталог</a></li>
					<li><a href="/news.php">Новости</a></li>
					<li><a href="/gb.php">Гостевая книга</a></li>
					<li><a href="/kontakty.php">Контакты</a></li>
				</ul>

			</td>
			<td id="content" valign="top">
				<h1><?=$pages['h1'];?></h1>
				<p>Адрес:&nbsp;107140, г. Москва, ул. Малая Красносельская, 7</p>

				<p>Телефон приемной:&nbsp;(499) 264-43-10</p>

				<p>Факс приемной:&nbsp;(499) 264-49-78</p>

				<p>Электронная почта приемной:&nbsp;<a href="mailto:info@babaev.ru">info@babaev.ru</a></p>

				<p>Телефоны отдела снабжения:&nbsp;(499) 264-99-79; 8-499-264-27-34; 982-57-11;</p>

				<p>982-57-10</p>

			</td>
		</tr>
		<tr>
			<td colspan="2"><hr></td>
		</tr>
		<tr id="footer">
			<td class="copy">2016 &copy; Кондитерский концерн "Бабаевский"</td>
		</tr>
	</table>
</body>
</html>